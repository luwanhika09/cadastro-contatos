import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:regcontact/src/config/custom_colors.dart';
import 'package:regcontact/src/pages/auth/controller/auth_controller.dart';
import 'package:regcontact/src/pages_routes/app_pages.dart';

void main() {
  Get.put(AuthController());

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(primarySwatch: CustomColors.customSwatchColor),
      initialRoute: PagesRoutes.splashRoute,
      getPages: AppPages.pages,
    );
  }
}
