import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:regcontact/src/config/custom_colors.dart';
import 'package:regcontact/src/pages_routes/app_pages.dart';

import '../../../config/responsive_widget/responsive_widget.dart';
import '../../common_widgets/app_name_widget.dart';
import '../../common_widgets/custom_text_field.dart';
import '../controller/auth_controller.dart';

class SignInScreen extends StatelessWidget {
  SignInScreen({super.key});

  final _formKey = GlobalKey<FormState>();

  // Contralador de campos
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: SizedBox(
          height: height,
          width: width,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                child: Container(
                  height: height,
                  color: CustomColors.customSwatchColor,
                  child: const Center(
                    child: AppNameWidget(
                      deepBlueTitleColor: Colors.white,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  height: height,
                  margin: EdgeInsets.symmetric(
                      horizontal: ResponsiveWidget.isMobileScaffold(context)
                          ? height * 0.032
                          : height * 0.12),
                  child: Container(
                    padding: const EdgeInsets.only(bottom: 40.0),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(height: height * 0.2),
                          RichText(
                            text: const TextSpan(
                              children: [
                                TextSpan(
                                  text: 'Let´s',
                                ),
                                TextSpan(
                                  text: ' Sign In 👇',
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: height * 0.02),
                          const Text(
                            'Hey, Enter your details to get sign in \nto your account.',
                          ),
                          SizedBox(height: height * 0.064),

                          // Email
                          CustomTextField(
                            controller: emailController,
                            icon: Icons.email,
                            label: 'Email',
                            validator: (email) {
                              if (email == null || email.isEmpty) {
                                return 'Digite seu email!';
                              }

                              if (!email.isEmail) {
                                return 'Digite um E-mail válido!';
                              }

                              return null;
                            },
                          ),

                          // Senha
                          CustomTextField(
                            controller: passwordController,
                            icon: Icons.lock,
                            label: 'Senha',
                            isSecret: true,
                            validator: (password) {
                              if (password == null || password.isEmpty) {
                                return 'Digite sua senha!';
                              }

                              if (password.length < 7) {
                                return 'Digite uma senha com pelo menos 7 caracteres!';
                              }

                              return null;
                            },
                          ),

                          // Botão de Entrar
                          SizedBox(
                            height: 50,
                            child: GetX<AuthController>(
                              builder: (authController) {
                                return ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                      shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18),
                                  )),
                                  onPressed: authController.isLoading.value
                                      ? null
                                      : () {
                                          FocusScope.of(context).unfocus();
                                          if (_formKey.currentState!
                                              .validate()) {
                                            String email = emailController.text;
                                            String password =
                                                passwordController.text;

                                            authController.signIn(
                                              email: email,
                                              password: password,
                                            );
                                          } else {
                                            print('Campos não válidos!');
                                          }

                                          // Get.toNamed(PagesRoutes.homeRoute);
                                        },
                                  child: authController.isLoading.value
                                      ? const CircularProgressIndicator()
                                      : const Text(
                                          'Entrar',
                                          style: TextStyle(
                                            fontSize: 18,
                                          ),
                                        ),
                                );
                              },
                            ),
                          ),

                          // Esqueceu a senha
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10),
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: TextButton(
                                onPressed: () {},
                                child: Text(
                                  'Esqueceu a senha?',
                                  style: TextStyle(
                                    color: CustomColors.customContrastColor,
                                  ),
                                ),
                              ),
                            ),
                          ),

                          // Divisor
                          Padding(
                            padding: const EdgeInsets.only(bottom: 10),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Divider(
                                    color: Colors.grey.withAlpha(90),
                                    thickness: 2,
                                  ),
                                ),
                                const Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 15),
                                  child: Text('Ou'),
                                ),
                                Expanded(
                                  child: Divider(
                                    color: Colors.grey.withAlpha(90),
                                    thickness: 2,
                                  ),
                                ),
                              ],
                            ),
                          ),

                          // Botão de novo usuário
                          SizedBox(
                            height: 50,
                            child: OutlinedButton(
                              style: OutlinedButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18),
                                ),
                                side: const BorderSide(
                                  width: 2,
                                  color: Colors.blueGrey,
                                ),
                              ),
                              onPressed: () {
                                Get.toNamed(PagesRoutes.signUpRoute);
                              },
                              child: const Text(
                                'Criar conta',
                                style: TextStyle(fontSize: 18),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
