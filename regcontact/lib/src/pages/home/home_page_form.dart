import 'package:flutter/material.dart';
import 'package:regcontact/src/pages/common_widgets/custom_text_field.dart';

class HomePageForm extends StatelessWidget {
  const HomePageForm({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(
          left: 100,
          right: 100,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(
              width: 400,
              child: CustomTextField(
                label: 'CPF',
                icon: Icons.copy,
                isSecret: true,
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            const CustomTextField(
              label: 'NOME',
              icon: Icons.person,
            ),
            const SizedBox(
              height: 15,
            ),
            const SizedBox(
              width: 400,
              child: CustomTextField(
                label: 'CEP',
                icon: Icons.numbers,
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            const CustomTextField(
              label: 'UF',
              icon: Icons.location_city,
            ),
            const SizedBox(
              height: 15,
            ),
            const CustomTextField(
              label: 'Cidade',
              icon: Icons.place,
            ),
            const SizedBox(
              height: 15,
            ),
            const CustomTextField(
              label: 'Logradouro',
              icon: Icons.roundabout_right,
            ),
            const SizedBox(
              height: 15,
            ),
            const CustomTextField(
              label: 'Bairro',
              icon: Icons.twenty_one_mp,
            ),
            SizedBox(
              height: 50,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18),
                  ),
                ),
                onPressed: () {},
                child: const Text(
                  'Adicionar contato',
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
