import 'package:flutter/material.dart';
import 'package:regcontact/src/config/custom_colors.dart';

import '../../../models/contato_model.dart';

class ContactTile extends StatelessWidget {
  final ContatoModel contactItem;

  const ContactTile({
    super.key,
    required this.contactItem,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.fromLTRB(20, 10, 20, 0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: ListTile(
        // Titulo
        title: Text(
          contactItem.name,
          style: const TextStyle(
            fontWeight: FontWeight.w500,
          ),
        ),

        // Descricao
        subtitle: Text(
          contactItem.cpf,
          style: TextStyle(
            color: CustomColors.customSwatchColor,
            fontWeight: FontWeight.bold,
            fontSize: 12,
          ),
        ),

        trailing: SizedBox(
          width: 80,
          child: Row(
            children: [
              IconButton(
                onPressed: () {},
                icon: const Icon(
                  Icons.edit,
                  color: Colors.amber,
                ),
              ),
              IconButton(
                onPressed: () {},
                icon: Icon(
                  Icons.delete,
                  color: CustomColors.customContrastColor,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
