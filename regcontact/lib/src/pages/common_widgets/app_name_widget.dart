import 'package:flutter/material.dart';

import '../../config/custom_colors.dart';

class AppNameWidget extends StatelessWidget {
  final Color? deepBlueTitleColor;
  const AppNameWidget({
    Key? key,
    this.deepBlueTitleColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text.rich(
      TextSpan(
        style: const TextStyle(
          fontSize: 40,
        ),
        children: [
           TextSpan(
            text: 'Cadastro',
            style: TextStyle(
              color: deepBlueTitleColor ?? CustomColors.customContrastColor,
              fontWeight: FontWeight.bold,
            ),
          ),
          TextSpan(
            text: 'contatos',
            style: TextStyle(color: CustomColors.customContrastColor),
          ),
        ],
      ),
    );
  }
}
