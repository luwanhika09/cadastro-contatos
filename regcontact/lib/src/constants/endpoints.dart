const String baseUrl = 'https://parseapi.back4app.com/functions/method';

abstract class Endpoints {

  static const String signin = '$baseUrl/login';
  static const String signup = '$baseUrl/signup';

}
