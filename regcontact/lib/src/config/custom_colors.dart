import 'package:flutter/material.dart';

Map<int, Color> _swatchOpacity = {
  50: const Color.fromRGBO(37, 43, 92, .1),
  100: const Color.fromRGBO(37, 43, 92, .2),
  200: const Color.fromRGBO(37, 43, 92, .3),
  300: const Color.fromRGBO(37, 43, 92, .4),
  400: const Color.fromRGBO(37, 43, 92, .5),
  500: const Color.fromRGBO(37, 43, 92, .6),
  600: const Color.fromRGBO(37, 43, 92, .7),
  700: const Color.fromRGBO(37, 43, 92, .8),
  800: const Color.fromRGBO(37, 43, 92, .9),
  900: const Color.fromRGBO(37, 43, 92, 1),
};

abstract class CustomColors {
  static const Color blueDarkColor = Color(0xFF252B5C);
  static Color customContrastColor = Colors.red.shade700;

  static MaterialColor customSwatchColor =
      MaterialColor(0xff252B5C, _swatchOpacity);
}
