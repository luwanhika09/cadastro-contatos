import 'package:flutter/material.dart';

class ResponsiveWidget extends StatelessWidget {
  final Widget? mobileScaffold;
  final Widget? tableScaffold;
  final Widget desktopScaffold;

  const ResponsiveWidget({
    super.key,
    required this.desktopScaffold,
    this.tableScaffold,
    this.mobileScaffold,
  });

  static bool isMobileScaffold(BuildContext context) {
    return MediaQuery.of(context).size.width < 600;
  }

  static bool isDesktopScaffold(BuildContext context) {
    return MediaQuery.of(context).size.width > 1200;
  }

  static bool isTableScaffold(BuildContext context) {
    return MediaQuery.of(context).size.width >= 600 &&
        MediaQuery.of(context).size.width <= 1200;
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth > 1200) {
          return desktopScaffold;
        } else if (constraints.maxHeight <= 1200 &&
            constraints.maxHeight >= 600) {
          return tableScaffold ?? desktopScaffold;
        } else {
          return mobileScaffold ?? desktopScaffold;
        }
      },
    );
  }
}
