import 'package:regcontact/src/models/contato_model.dart';

ContatoModel contato = ContatoModel(
  name: 'Luwanhika',
  cpf: '23698101835',
  phone: '14981017136',
  address: [endereco],
  latitude: 123547,
  longitude: 09876,
);

Address endereco = Address(
  uf: 'SP',
  cidade: 'Lins',
  endereco: 'Cônego Vicente',
);

List<ContatoModel> listCont = [
  contato,
];
