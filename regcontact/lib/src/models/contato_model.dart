
class ContatoModel {
  String name;
  String cpf;
  String phone;
  List<Address>? address;
  double latitude;
  double longitude;

  ContatoModel({
    required this.name,
    required this.cpf,
    required this.phone,
    required this.address,
    required this.latitude,
    required this.longitude,
  });
}

class Address {
  String? uf;
  String? cidade;
  String? endereco;

  Address({
    this.uf,
    this.cidade,
    this.endereco,
  });
}
